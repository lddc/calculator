package com.leszko.calculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;                                                  

@RestController
public class CalculatorController {
    @Autowired
    private Calculator calculator;

    @RequestMapping("/sum")
    public String sum(@RequestParam("a") Integer a, @RequestParam("b") Integer b) {
        return String.valueOf(calculator.sum(a, b));
    }

    @RequestMapping("/sub")
    public String sub(@RequestParam("a") Integer a, @RequestParam("b") Integer b) {
        return String.valueOf(calculator.sub(a, b));
    }

    /*@RequestMapping("/div")
    public String div(@RequestParam("a") Integer a, @RequestParam("b") Integer b) {                                                                                          
        return String.valueOf(calculator.div(a, b));
    }

    @RequestMapping("/mult")
    public String mult(@RequestParam("a") Integer a, @RequestParam("b") Integer b) {
        return String.valueOf(calculator.mult(a, b));
    }*/
}
